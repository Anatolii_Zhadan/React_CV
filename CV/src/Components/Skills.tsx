interface Info {
  name: string;
  arraySkills: string[];
}

interface ArrayInfo {
  header: string;
  arrayInfo: Info[];
}

export interface Array {
  array: ArrayInfo[];
}

function Skills(props: Array) {
  return (
    <div className="container">
      <hr className="border-2 opacity-50 border-black" />
      {props.array.map((array, index) => (
        <div key={"header" + index} className="row">
          {array.header.length !== 0 && <h2>{array.header}</h2>}
          {array.arrayInfo.map((arraySkills, index) => (
            <div key={"container" + index} className="container col">
              {arraySkills.name.length !== 0 && <h5>{arraySkills.name}</h5>}
              <ul className="list-group">
                {arraySkills.arraySkills.map((skills, index) => (
                  <li key={index} className="list-unstyled">
                    {skills}
                  </li>
                ))}
              </ul>
            </div>
          ))}
        </div>
      ))}
    </div>
  );
}

export default Skills;
