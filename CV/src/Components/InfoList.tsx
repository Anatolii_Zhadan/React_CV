export interface Info {
  header: string;
  title: string;
  additionalInfo: string;
  secondAdditional: string;
  link: string;
}

interface InfoArray {
  info: Info[];
  heading: string;
}

function InfoList(props: InfoArray) {
  const EmptyCheck = () => {
    return props.info.length === 0 && <h3 className="ms-2">No data</h3>;
  };

  return (
    <div className="container">
      <hr className="border-2 opacity-50 border-black" />
      <div className="list-group">
        {props.heading.length !== 0 && <h2>{props.heading}</h2>}
        {EmptyCheck()}
        {props.info.map((info, index) => (
          <div className="mx-4 mb-3" key={"info-case " + index}>
            <div className="d-flex w-100 justify-content-between">
              {info.link.length !== 0 ? (
                <a
                  href={info.link}
                  className="link-secondary link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover"
                >
                  <h5>{info.header}</h5>
                </a>
              ) : (
                <h5>{info.header}</h5>
              )}
              <small>{info.additionalInfo}</small>
            </div>
            <div className="d-flex w-100 justify-content-between">
              <small className="me-2 w-25">{info.secondAdditional}</small>
              <p className="mb-1">{info.title}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default InfoList;
