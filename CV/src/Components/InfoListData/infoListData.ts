import { Info } from "../InfoList";

export const educationData: Info[] = [
  {
    header: "42 Lisbon",
    title:
      "Currently, I'm refining my programming abilities at 42 School in Lisbon, a globally acclaimed \
      institution known for its innovative approach to coding education. Here, we foster our secondAdditional through \
      teamwork on practical projects, evaluated by an engaging gamification system that propels both individual \
      development and collaborative learning.",
    additionalInfo: "2023-now",
    secondAdditional: "C, C++, Git, Shell",
    link: "https://www.42lisboa.com/en/",
  },
  {
    header: "RS School",
    title:
      "Previously, I attended RS School, where I deepened my knowledge of HTML, CSS, and JavaScript. \
      Known for its industry-relevant curriculum and strong ties with tech companies, RS School provides an \
      outstanding educational experience. The practical projects I worked on enhanced my understanding and \
      application of web development secondAdditional.",
    additionalInfo: "2022-2023",
    secondAdditional: "C#",
    link: "https://rs.school/",
  },
  {
    header: "Udemy's C# for Unity course",
    title:
      "With a goal to create my own game, I took Udemy's C# for Unity course. This practical, project-oriented \
      course honed my understanding of C# and Unity, effectively paving the way for my journey in game development. The \
      hands-on experience significantly enhanced my secondAdditional, bringing me closer to my gaming aspirations.",
    additionalInfo: "2020-2021",
    secondAdditional: "C#, Unity",
    link: "",
  },
  {
    header: "Lyceum of Information Technologies",
    title:
      "Prior to my current studies, I had the opportunity to study C# at the Lyceum of Information Technologies \
      in Dnipro, a well-respected institution known for its innovative IT education and strategic alliance with Microsoft. \
      This experience culminated in my development of a fully functional PC software program using C#, highlighting the practical, \
      hands-on nature of the curriculum at this esteemed institution.",
    additionalInfo: "2019-2020",
    secondAdditional: "C#",
    link: "https://www.dlit.dp.ua/",
  },
  {
    header: 'It school "SMART"',
    title:
      'I spent a year attending the "Smart" IT school, where I dedicated three hours weekly to learn the syntax and \
      functionalities of Python. Known for its immersive approach to IT education, this institution fostered my understanding \
      and practical usage of Python. By the end of my tenure there, I successfully passed all requisite examinations, showcasing \
      my comprehensive grasp of this powerful programming language.',
    additionalInfo: "2018-2019",
    secondAdditional: "Python",
    link: "https://uaitsmart.com/",
  },
];

export const projectData = [
  {
    header: "CV on React",
    title:
      "Delve into my comprehensive online CV, meticulously developed using Bootstrap, TypeScript, and React.",
    additionalInfo: "Explore code of site that you see now",
    secondAdditional: "React, Bootstrap, TypeScript",
    link: "https://gitlab.com/Anatolii_Zhadan/React_CV",
  },
  {
    header: "Telegram Redirector Site",
    title:
      "This web application collects user information through a form, stores it in a secure database, and sends the data to an administrator on Telegram using a bot. Technologies used include Python, SQLite, and Flask.",
    additionalInfo:
      "Telegram Redirector Site: Instant data redirection to Telegram with secure database storage",
    secondAdditional: "Python, SQLite, Flask",
    link: "https://github.com/ImmuneFOMO/TelegramRedirectorSite",
  },
  {
    header: "Philosophers",
    title:
      "In this project, I honed my secondAdditional working with Pthread and Mutex to craft a solution to the Dining Philosophers problem. In the bonus section, I ventured further by incorporating Fork and Semaphores, gaining a deep understanding of these technologies.",
    additionalInfo:
      "Solved Dining Philosophers with threads/mutex and fork/semaphores",
    secondAdditional: "C, Pthread, Mutex, Fork, Semaphores",
    link: "https://gitlab.com/Anatolii_Zhadan/Philosophers",
  },
  {
    header: "Push Swap",
    title:
      "I designed an efficient algorithm for sorting stacks, which involved a deep understanding of data structures optimization and balancing between time and space complexities. The algorithm operates on two stacks and exemplifies my ability to tackle complex problems and optimize algorithms.",
    additionalInfo: "Developed an efficient stack sorting algorithm",
    secondAdditional: "C, Data Structures, Algorithm Optimization",
    link: "https://gitlab.com/Anatolii_Zhadan/push_swap",
  },
  {
    header: "So Long",
    title:
      "This project involved creating a captivating 2D collectible game using the MiniLibX graphics library. I engaged in the entire development process, from conceptualizing game rules to handling user input for character movement, ensuring smooth game operation and efficient memory management.",
    additionalInfo: "Developed a 2D game using MiniLibX",
    secondAdditional: "C, MiniLibX, Game Design, Graphics Programming",
    link: "https://gitlab.com/Anatolii_Zhadan/so_long",
  },
  {
    header: "Minitalk",
    title:
      "I created a secure client-server communication system using UNIX signals. The project required understanding of UNIX system calls, process handling, and interprocess communication. I utilized SIGUSR1 and SIGUSR2 signals for binary data translation.",
    additionalInfo:
      "Implemented a client-server communication system using UNIX signals",
    secondAdditional: "C, UNIX Signals, Client-Server Communication",
    link: "https://gitlab.com/Anatolii_Zhadan/minitalk",
  },
  {
    header: "Libft",
    title:
      "The 'Libft' project involved crafting a C library with reusable functions, including utilities for file handling and linked list operations. I recreated several standard C library functions and developed linked list tools, showcasing my proficiency in C programming and data structures.",
    additionalInfo: "Developed a comprehensive library in C",
    secondAdditional: "C, File Handling, Linked Lists",
    link: "https://gitlab.com/Anatolii_Zhadan/libft",
  },
  {
    header: "Ft_Printf",
    title:
      "I reimplemented the printf function in C, supporting a subset of printf's conversion specifiers and managing formatting flags. This project emphasized my strong grasp of C programming, particularly in variadic functions, string parsing, and output formatting.",
    additionalInfo: "Reimplemented the printf function in C",
    secondAdditional: "C, Variadic Functions, String Parsing",
    link: "https://gitlab.com/Anatolii_Zhadan/ft_printf",
  },
  {
    header: "Get_Next_Line",
    title:
      "I developed a C function capable of reading any size line from a file, involving careful memory management and file manipulation. The function, designed to be called in a loop, reads an open file descriptor line by line.",
    additionalInfo: "Developed a C function to read any size line from a fd",
    secondAdditional: "C, File Operations, Memory Management",
    link: "https://gitlab.com/Anatolii_Zhadan/get_next_line",
  },
  {
    header: "QR Code Hunt Challenge",
    title:
      "Developed a Node.js-based web application for a QR code treasure hunt. Utilizing Express.js, EJS, and Path, I built a system that verifies request codes server-side and generates hints for the next QR location.",
    additionalInfo:
      "This is a Node.js project that leverages Express.js, EJS and Path",
    secondAdditional: "Node.js, Express.js, EJS, Path",
    link: "https://github.com/ImmuneFOMO/qr_code_hunt_challenge",
  },
  {
    header: "Flexbox Examples",
    title:
      "This project showcases examples of using Flexbox in CSS. It includes a website where users can explore different use cases of Flexbox and customize parameters for each property.",
    additionalInfo: "showcasing examples of using Flexbox in CSS",
    secondAdditional: "HTML, CSS, Flexbox",
    link: "https://github.com/ImmuneFOMO/Flexbox-examples",
  },
  {
    header: "Plants Store Landing",
    title:
      "I designed an adaptive website for a plant store using CSS, HTML, and JavaScript. The site features a valid semantic layout optimized for various screen resolutions and incorporates JavaScript for interactivity.",
    additionalInfo:
      "Developed an adaptive website using CSS, HTML, and JavaScript",
    secondAdditional: "HTML, CSS, JavaScript, Responsive Web Design",
    link: "https://gitlab.com/Anatolii_Zhadan/PlantsCssHtml",
  },
];
