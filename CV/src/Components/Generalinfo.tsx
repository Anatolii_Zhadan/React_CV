export interface GeneralInfo {
  firstName: string;
  secondName: string;
  gmail: string;
  phoneNum: number;
  imgLink: string;
}

function GeneralInfo(props: GeneralInfo) {
  return (
    <div className="container">
      <div className="row align-items-center">
        <div className="col-auto">
          <img
            className="img-thumbnail mt-2"
            src={props.imgLink}
            alt={props.firstName + " " + props.secondName}
            style={{ maxWidth: '200px', maxHeight: '200px' }}
          />
        </div>
        <div className="col">
          <h1>{props.firstName + " " + props.secondName}</h1>
          <p>{"Email: " + props.gmail}</p>
          <p>{"Phone number: +" + props.phoneNum}</p>
        </div>
      </div>
    </div>
  );
}

export default GeneralInfo;
