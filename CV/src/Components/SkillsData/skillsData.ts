import {Array} from "../Skills";

export const exampleSkillsData: Array = {
    array: [
        {
            header: "Skills",
            arrayInfo: [
                {
                    name: "Languages:",
                    arraySkills: ["C", "C++", "HTMl, CSS, JS", "Python", "C#", "TypeScript", "Go"]
                },
                {
                    name: "Technologies:",
                    arraySkills: ["Docker", "React.js", "Node.js", "Bootstrap", "Express.js"]
                },
                {
                  name: "Other:",
                  arraySkills: ["Git", "Shell", "Package managers"]
                }
            ]
        },
    ],
};