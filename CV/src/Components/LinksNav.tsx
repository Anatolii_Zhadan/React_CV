export interface Link {
  pic: string;
  link: string;
  name: string;
}

interface LinkArray {
  links: Link[];
  heading: string;
}

function LinksNav(props: LinkArray) {
  return (
    <div className="container">
      <hr className="border-2 opacity-50 border-black" />
      {props.heading.length !== 0 && <h2>{props.heading}</h2>}
      <nav className="navbar">
        <div className="container-fluid d-flex justify-content-center">
          {props.links.map((link, index) => (
            <a
              key={index}
              className="navbar-brand d-flex align-items-center me-3 link-secondary link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover"
              target="_blank"
              href={link.link}
            >
              <img
                src={link.pic}
                alt={"logo_" + link.name}
                width="60"
                max-height="48"
                className="d-inline-block align-text-top me-1"
              />
              {link.name}
            </a>
          ))}
        </div>
      </nav>
    </div>
  );
}

export default LinksNav;
