interface InfoBlock {
  header: string;
  text: string;
}

function InfoBlock(props: InfoBlock) {
  return (
    <div className="container">
        <hr className="border-2 opacity-50 border-black"/>
      <h2>{props.header}</h2>
      <p>{props.text}</p>
    </div>
  );
}

export default InfoBlock;
