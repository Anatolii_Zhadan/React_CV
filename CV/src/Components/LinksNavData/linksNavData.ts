import imgHub from "../../assets/social_svg/github.svg";
import imgLab from "../../assets/social_svg/GitLab.svg";
import imgLinked from "../../assets/social_svg/LinkedIn.svg";
import imgMedium from "../../assets/social_svg/Medium.svg";
import imgHackern from "../../assets/social_svg/Hackern.svg";
import {Link} from "../LinksNav";

export const linksArray: Link[] = [
    {
      pic: imgHub,
      link: "https://github.com/ImmuneFOMO",
      name: "Github",
    },
    {
      pic: imgLab,
      link: "https://gitlab.com/Anatolii_Zhadan",
      name: "Gitlab",
    },
    {
      pic: imgMedium,
      link: "https://medium.com/@Anatolii_Zhadan",
      name: "Medium",
    },
    {
      pic: imgHackern,
      link: "https://hackernoon.com/u/zhadan",
      name: "HackerNoon",
    },
    {
      pic: imgLinked,
      link: "https://www.linkedin.com/in/anatolii-zhadan/",
      name: "LinkedIn",
    },
  ];