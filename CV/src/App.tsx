import GeneralInfo from "./Components/Generalinfo";
import InfoBlock from "./Components/InfoBlock";
import LinksNav from "./Components/LinksNav";
import InfoList from "./Components/InfoList";
import { educationData, projectData } from "./Components/InfoListData/infoListData";
import { linksArray } from "./Components/LinksNavData/linksNavData";
import Skills from "./Components/Skills";
import { exampleSkillsData } from "./Components/SkillsData/skillsData";

function App() {

  return (
    <>
      <GeneralInfo
        firstName="Anatolii"
        secondName="Zhadan"
        gmail="rikiton.shift@gmail.com"
        phoneNum={+351935524919}
        imgLink="https://i.ibb.co/CMGr7qM/IMG-2890.jpg"
      />
      <Skills {...exampleSkillsData}/>
      <InfoBlock
        header="About me"
        text="Hello there! My name is Anatoly and I have been exploring technology since the age of 14. Over the years, I have developed various skills such as game development using Unity and C#, website creation using HTML, CSS, and JavaScript, and online automation using Python. I'm currently attending 42 Lisbon, a well-known private institution of higher education worldwide, where I'm deepening my understanding of the C and C++ languages to further enhance my programming toolkit. Although I am originally from Ukraine, I am now based in Lisbon, Portugal, and am always eager to explore new and innovative global tech trends. I'm currently learning Portuguese, Go, and React as hobbies."
      />
      <LinksNav links={linksArray} heading="My Links" />
      <InfoList info={educationData} heading="Education" />
      <InfoList info={projectData} heading="Projects" />
    </>
  );
}

export default App;